(define (S n z) // S is successor
	  (define (iter n z)
	    (if (zero? n)
	        z
	        (iter (- n 1) (+ z 1))))
	  (iter n z))
	  
(define (P n z)// P is predecessor
	  (define (iter n z)
		(if (zero? z)
			z
			(if (zero? n)
				z
				(iter (- n 1) (- z 1)))))
	  (iter n z))
(define ADD(lambda (x y)         
		(S x (S y 0)) ))
		 
(define SUB(lambda (x y)         
		(P y x) ))
		
		(define TRUE(lambda (x y)         
		 x ))
		 
(define FALSE(lambda (x y)         
		 y ))
		
(define AND(lambda (M N)         
		(N (M "TRUE" "FALSE") "FALSE")))
		 
(define Or(lambda (M N)         
		 (N "TRUE" (M "TRUE" "FALSE"))))
		 
(define Not(lambda (M)         
		 (M "FALSE" "TRUE")))
		 
(define LEQ(lambda (x y)       
		(ISZERO (SUB x y)) ))

(define GEQ(lambda (x y)       
		(ISZERO (SUB y x)) ))
		 

	  
(define (ISZERO n)
	  (if (zero? n)
		"TRUE"
		"FALSE"
	  ))